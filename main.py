import vk_api
from random import randint
from datetime import datetime
from vk_api.utils import get_random_id
import requests
import json
import re

vk = vk_api.VkApi(token="b607705f5b15fe48b01366a1a070ad6e66e5e49fe5b748d12eabaa55572fedac112cd81ee8fadd1449269")

vk._auth_token()
d = datetime.now()
print("VK-bot by Timofey Chernenko")
if d.minute < 10:
    print("Время запуска:" + str(d.hour) + ":0" + str(d.minute))
else:
    print("Время запуска:" + str(d.hour) + ":" + str(d.minute))

kbrd = open("C:/Users/lenovo/PycharmProjects/vk_bot/keyboard.json", "r", encoding="UTF-8").read()


def send_answer(m, k=kbrd):
    t = datetime.now()
    vk.method("messages.setActivity", {"type": "typing", "peer_id": id, "random_id": get_random_id()})
    vk.method("messages.send",
              {"peer_id": id, "message": m, "keyboard": k, "random_id": get_random_id()})
    if t.minute < 10:
        print(str(t.hour) + ":0" + str(t.minute) + "|Бот ответил:" + m)
    else:
        print(str(t.hour) + ":" + str(t.minute) + "|Бот ответил:" + m)


def send_mailing(m):
    t = datetime.now()
    ids = list(ids_data.values())
    print(ids)
    vk.method("messages.send",
              {"peer_id": ids, "message": m, "keyboard": kbrd, "random_id": get_random_id()})
    if t.minute < 10:
        print(str(t.hour) + ":0" + str(t.minute) + "|Бот ответил:" + m)
    else:
        print(str(t.hour) + ":" + str(t.minute) + "|Бот ответил:" + m)


def send_sticker():
    t = datetime.now()
    fruit_stickers = (
        134, 140, 145, 136, 143, 151, 148, 144, 142, 137, 135, 133, 138, 156,
        150,
        153, 149, 147, 141, 159, 164, 161, 130, 132, 160, 146, 152, 139, 155,
        131,
        154, 162, 163, 158, 167, 129, 166, 168, 157, 165)
    i = randint(0, len(fruit_stickers) - 1)
    vk.method("messages.send",
              {"peer_id": id, "sticker_id": fruit_stickers[i], "keyboard": kbrd,
               "random_id": get_random_id()})
    if t.minute < 10:
        print(str(t.hour) + ":0" + str(t.minute) + "|Бот отправил стикер")
    else:
        print(str(t.hour) + ":" + str(t.minute) + "|Бот отправил стикер")


def get_random():
    mes = "Введите минимальное и максимальное число через пробел"
    send_answer(mes)
    while True:
        ans = vk.method("messages.getConversations", {"offset": 0, "count": 20, "filter": "unanswered"})
        if ans["count"] >= 1:
            idr = ans["items"][0]["last_message"]["from_id"]
            bodyr = ans["items"][0]["last_message"]["text"]
            try:
                inp = re.split(' ', bodyr)
                mn = int(inp[0])
                mx = int(inp[1])
                mes = "Ваше число:" + str(randint(mn, mx))
                vk.method("messages.send",
                          {"peer_id": idr, "message": mes, "keyboard": kbrd, "random_id": get_random_id()})
                print("Бот отправил сообщение:" + mes)
                break
            except ValueError:
                print("Юзер пидарас")
                mes = "Числа введены некорректно!Попробуйте ещё раз"
                vk.method("messages.send",
                          {"peer_id": idr, "message": mes, "keyboard": kbrd, "random_id": get_random_id()})
                get_random()
                break


def send_photo():
    ph_kbrd = open("C:/Users/lenovo/PycharmProjects/vk_bot/photo_keyboard.json", "r", encoding="UTF-8").read()
    ms = "Выберите одну из категорий: Животные, Природа, Технологии"
    send_answer(ms, ph_kbrd)
    while True:
        ans = vk.method("messages.getConversations", {"offset": 0, "count": 20, "filter": "unanswered"})
        if ans["count"] >= 1:
            idp = ans["items"][0]["last_message"]["from_id"]
            bodyp = ans["items"][0]["last_message"]["text"]
            print(bodyp)
            if bodyp.lower() == "животные":
                i = randint(1, 10)
                a = vk.method("photos.getMessagesUploadServer")
                b = requests.post(a['upload_url'], files={
                    'photo': open('C:/Users/lenovo/PycharmProjects/vk_bot/photos/animals/' + str(i) + '.jpg',
                                  'rb')}).json()
                c = \
                    vk.method('photos.saveMessagesPhoto',
                              {'photo': b['photo'], 'server': b['server'], 'hash': b['hash']})[
                        0]
                d = "photo{}_{}".format(c["owner_id"], c["id"])
                vk.method("messages.send",
                          {"peer_id": idp, "message": "лови)0)", "keyboard": ph_kbrd, "attachment": d, "random_id": 0})
                print("Бот отправил фото")
                break
            else:
                m = "Штурман,у нас проблемы"
                send_answer(m)


with open("C:/Users/lenovo/PycharmProjects/vk_bot/users_ids.json", "r", encoding='utf-8') as f:
    ids_data = json.load(f)

while True:
    messages = vk.method("messages.getConversations", {"offset": 0, "count": 20, "filter": "unanswered"})
    if messages["count"] >= 1:
        id = messages["items"][0]["last_message"]["from_id"]
        user = vk.method("users.get", {"user_ids": id})
        fullname = user[0]['first_name'] + ' ' + user[0]['last_name']
        ids_data[fullname] = id
        with open("C:/Users/lenovo/PycharmProjects/vk_bot/users_ids.json", "w", encoding='utf-8') as f:
            json.dump(ids_data, f, indent=2, ensure_ascii=False)
        body = messages["items"][0]["last_message"]["text"]
        print("Получено сообщение от " + fullname + ":" + body)

        if body.lower() == "привет" or body.lower() == "приветик" or body.lower() == "приветос" \
                or body.lower() == "хай" or body.lower() == "хэллоу" or body.lower() == "хэлоу" \
                or body.lower() == "здарова" or body.lower() == "ну здарова" or body.lower() == "ку":
            choose = randint(1, 4)
            if choose == 1:
                message = "Ну здарова"
                vk.method("messages.setActivity",
                          {"type": "typing", "peer_id": id, "random_id": get_random_id()})
                vk.method("messages.send",
                          {"peer_id": id, "message": message, "random_id": get_random_id()})
                print("Бот ответил:" + message)
            if choose == 2:
                message = "Хэллоу :)"
                vk.method("messages.setActivity",
                          {"type": "typing", "peer_id": id, "random_id": randint(1, 2147483647)})
                vk.method("messages.send",
                          {"peer_id": id, "message": message, "random_id": randint(1, 2147483647)})
                print("Бот ответил:" + message)
            if choose == 3:
                message = "Приветик)0"
                vk.method("messages.setActivity",
                          {"type": "typing", "peer_id": id, "random_id": randint(1, 2147483647)})
                vk.method("messages.send",
                          {"peer_id": id, "message": message, "random_id": randint(1, 2147483647)})
                print("Бот ответил:" + message)
            if choose == 4:
                message = "Хай)"
                vk.method("messages.setActivity",
                          {"type": "typing", "peer_id": id, "random_id": randint(1, 2147483647)})
                vk.method("messages.send",
                          {"peer_id": id, "message": "Хай)", "random_id": randint(1, 2147483647)})
                print("Бот ответил:" + message)
        elif body.lower() == "пердани-ка мне анекдот какой-нибудь" or body.lower() == "давай анекдот":
            message = "Заплутал какой-то в лесу джин,а медведь с зайцем его поймали,ну он и говорит:Загадывайте" \
                      + "три любых желания, и тут медведь говорит:Я хочу,чтобы хуй заебись стоял,джин почесал репу,щелкнул пальцами" \
                      + " и желание исполнилось,теперь заяц подумал,подумал и говорит:Хочу самый модный костюм мотоциклиста и чтоб под мой размерчик," \
                      + "джин щелкнул пальцем и заяц теперь в новом костюме,теперь медведь говорит:Хочу чтобы все медведи в этом лесу стали медведицами,джин почесал репу,щелкнул пальцами," \
                      + "и желание исполнилось,потом заяц добавляет:Теперь хочу самый новый и быстрый моцик,который только существует, ну джин опять щелкнул и мотоцикл появился" \
                      + ",дальше медведь говорит:А знаете что,я хочу,чтобы вообще все медведи в мире,кроме меня,стали медведицами, джин увидился,но что делать,щелкнул пальцами" \
                      + "и всё исполнил,и напоследок спрашивает зайца:А какое твоё последнее желание?, а заяц отвечает:" \
                      + "Хочу,чтобы медведь стал пидарасом, садится на моцик и уезжает )0)"
            vk.method("messages.setActivity", {"type": "typing", "peer_id": id, "random_id": randint(1, 2147483647)})
            vk.method("messages.send",
                      {"peer_id": id, "message": message, "random_id": randint(1, 2147483647)})
            print("Бот отправил анекдот")
        elif body.lower() == "как дела?" or body.lower() == "как дела":
            message = "всё харашо)00"
            send_answer(message)
        elif body.lower() == "начать":
            message = "ну давай начнём)0"
            send_answer(message)
        elif body.lower() == "рандом":  # рандомайзер
            get_random()
        elif body.lower() == "фото":
            send_photo()
        elif body.lower() == "что делаешь?" or body.lower() == "что делаешь":
            message = "тебе отвечаю,что ещё делать-то блинб"
            send_answer(message)
        elif body.lower() == "ясно" or body.lower() == "понятно":
            message = "а я вот ничего не понял(9"
            send_answer(message)
        elif body.lower() == "на связи":
            message = "До связи"
            send_answer(message)
        elif body.lower() == "позови братву":
            message = "ЭУЭУЭУЭУЭУ"
            send_mailing(message)
        elif body.lower() == "как жизнь?" or body.lower() == "как жизнь":
            message = "Я просто бот,какая жизньб..."
            send_answer(message)
        elif body.lower() == "бот,кто твой краш?" or body.lower() == "бот, кто твой краш?":  # отправка фото
            a = vk.method("photos.getMessagesUploadServer")
            b = requests.post(a['upload_url'], files={
                'photo': open('C:/Users/lenovo/PycharmProjects/vk_bot/photos/milos.jpg', 'rb')}).json()
            c = vk.method('photos.saveMessagesPhoto', {'photo': b['photo'], 'server': b['server'], 'hash': b['hash']})[
                0]
            d = "photo{}_{}".format(c["owner_id"], c["id"])
            vk.method("messages.send", {"peer_id": id, "message": "лови)0)", "attachment": d, "random_id": 0})
            print("Бот отправил фото")
        elif body.lower() == "слыш" or body.lower() == "слышь":
            message = "чкв?"
            send_answer(message)
        elif body.lower() == "понимаю" or body.lower == "панимаю" or body.lower == "панемаю":
            message = "Понимааю"
            send_answer(message)
        elif body.lower() == "я тебя люблю":
            message = "Ну ладно"
            send_answer(message)
        elif body.lower() == "не понимаю" or body.lower() == "ни панимаю" or body.lower() == "не панимаю":
            message = "Я тоже ничиго нипанимаю"
            send_answer(message)
        else:
            choose = randint(1, 4)
            if choose == 1:
                message = "Больше мне нечего добавить"
                send_answer(message)
            if choose == 2:
                message = "И ведь не поспоришь"
                send_answer(message)
            if choose == 3:
                message = "И не говори"
                send_answer(message)
            if choose == 4:
                send_sticker()

# Грусна и нифкусна(((
